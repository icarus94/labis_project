<?php 
	include 'functions.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="author" content="Trajko">
		<meta charset="UTF-8">
		<title>Studentska administracija</title>
		<link rel="stylesheet" type="text/css" href="css/mycss.css">
	</head>
	<body>
		<center>
			<div>
				<h1>Forma za unos podataka o studentu:</h1>
			</div>
		</center>
		<center>
			<div id="firstForm">
				<?php
					generateFirstForm("student","Ime","Prezime","Broj indeksa","Mesto prebivališta");
				?>
			</div>
		</center>	
		<center>
			<div id="secondForm">
				<h1>Položeni ispiti</h1>
				<?php
					generateTable("RB","NAZIV PREDMETA","OCENA");
				?>
			</div>
		</center>

		<center>
			<div id="thirdForm">
				<h1>Unos položenih predmeta</h1>
				<?php
					generateSecondForm("Naziv predmeta","Ocena");
				?>
			</div>
		</center>
		<?php 
			getStudentData();
		?>
	</body>
</html>